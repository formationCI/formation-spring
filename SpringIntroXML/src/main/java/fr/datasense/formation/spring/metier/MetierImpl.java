package fr.datasense.formation.spring.metier;

import org.springframework.beans.factory.InitializingBean;

import fr.datasense.formation.spring.dao.IDao;

public class MetierImpl implements IMetier,InitializingBean{
	

	private IDao dao;
	
	public double calculPrixTTC() {
		return dao.getPrixHT() * 1.2d;
	}


	public void setDao(IDao dao) {
		this.dao = dao;
	}


	public void afterPropertiesSet() throws Exception {
		System.out.println("init classe MetierImp");
		
	}
	
	
}
