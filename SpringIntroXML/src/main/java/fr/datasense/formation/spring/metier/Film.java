package fr.datasense.formation.spring.metier;

import javax.annotation.PostConstruct;

public class Film {

	private String nom;
	private int duree;

	public Film() {

	}

	public Film(String nom, int duree) {
		super();
		this.nom = nom;
		this.duree = duree;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getDuree() {
		return duree;
	}

	public void setDuree(int duree) {
		this.duree = duree;
	}

	public void initIt() throws Exception {
		System.out.println("Init method FILM");
	}	
	
	@PostConstruct
	public void initialiseAvecAnnotation() {
		System.out.println("Init method FILM : initialiseAvecAnnotation()  ");
	}
	
	public void cleanUp() throws Exception {
		System.out.println("Spring Container destroy");
	}

	@Override
	public String toString() {
		return "Film [nom=" + nom + ", duree=" + duree + "]";
	}

}
