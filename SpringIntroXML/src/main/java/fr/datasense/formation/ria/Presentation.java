package fr.datasense.formation.ria;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.datasense.formation.spring.metier.Film;
import fr.datasense.formation.spring.metier.IMetier;
/**
 * les réf : 
 * https://docs.spring.io/spring/docs/4.3.9.RELEASE/spring-framework-reference/htmlsingle/
 * https://docs.spring.io/spring/docs/4.3.9.RELEASE/spring-framework-reference/htmlsingle/#beans-factory-scopes
 * https://docs.spring.io/spring/docs/4.3.9.RELEASE/spring-framework-reference/htmlsingle/#beans-factory-lazy-init
 https://docs.spring.io/spring/docs/4.3.9.RELEASE/spring-framework-reference/htmlsingle/#beans-factory-autowire
 * @author ismael
 *
 */
public class Presentation {

	public static void main(String[] args) {

		
		
		
		//chargement du context spring avec  ApplicationContext
		ApplicationContext context = new ClassPathXmlApplicationContext("fr/datasense/formation/conf/applicationContext.xml");
//		==========
//		EXO 1
//		==========
//		Creer et recuperation un bean IMetier "metierBean", via le context et afficher le prix de ce bean
//		Précision : L'Injection de doa se fera par référence. Il faudra donc au préalable creer un bean Dao :-) 

		IMetier metier = (IMetier) context.getBean("metierBean");
		System.out.println(metier.toString() + " "+ metier.calculPrixTTC());
		 
	
//		recuperer le meme bean Metier via le context puis faite le même affichage
		IMetier metier2 = (IMetier) context.getBean("metierBean2");
		System.out.println(metier2.toString() + " "+ metier2.calculPrixTTC());
		
		IMetier metier3 = (IMetier) context.getBean("metierBean");
		System.out.println(metier3.toString() + " "+ metier3.calculPrixTTC());
		
		
//		Question : que represente metier.toString() et metier2.toString()? 
//      expliquez pourquoi 2 resultats different 
//		==========
//		EXO 2
//		==========
//		Configurer le bean "metierBean" pour que le toString ne soit plus egaux
		
		
		
//		==========
//		EXO 3
//		==========
		
//		Creer et recuperer  un bean Film "filmBean"  en injectant un  nom de  film (ex : zorro)
//		 resultat attendu en faisant System.out.println("FilmBean :" + f.getNom()); => FilmBean : zoro
		Film f = (Film) context.getBean("filmBean");
		System.out.println("FilmBean :" + f.getNom());
		
		
//		Creer recuperer un autre bean Film "filmBean2" en injectant cette fois le nom ainsi que la dureee en argument du bean 
//		resultat attendu en faisant System.out.println("FilmBean :" + f); => Film [nom=X-Man, duree=140]
		Film f2 = (Film) context.getBean("filmBean2");
		System.out.println("FilmBean2 :" + f2);
		
		//Creer un autre bean Film  "filmBean3" sans le recuperer. Dans sa création faite en sorte d'invoquer la méthode 
		//"initIt  sans faire implementer la classe Film de InitializingBean :-)

//		==========		
//		EXO 4
//		==========
//		Creer un nouveau  bean dao "daoBean2"  et un nouveau bean metier "metierBean2".
//		Dans bean metier2 explorer l'injection autowire par Type.
//		Pourquoi ca ne marche pas ?
		
//		IMetier metier4 = (IMetier) context.getBean("metierBean3");
//		System.out.println(metier4.toString() + " "+ metier4.calculPrixTTC());
		
//		Rajouter la propriété lazy-init à beanMetier2 en testant les différentes valeur possible.
//		Expliquez 
		
//		Faites en sorte que le code ci dessous puisse marcher en utilisant metierBean2. 
//		Aide  : Il faudra ici utiliser l'injection autowired  par Nom d'un nouveau bean Dao
		
//	IMetier metier3 = (IMetier) context.getBean("metierBean2");
//	System.out.println(metier3.toString() + " "+ metier3.calculPrixTTC());
		
		
//		==========		
//		EXO 4
//		==========
//		Définir un bean Spring pour exploiter  l'annotation @PostConstruc de la classe Film sans utiliser le component-scan
//		https://docs.spring.io/spring/docs/4.3.9.RELEASE/spring-framework-reference/htmlsingle/#beans-factory-extension-bpp
		
		
	}

}
