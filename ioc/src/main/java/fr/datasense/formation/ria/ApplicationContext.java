package fr.datasense.formation.ria;

import java.io.IOException;
import java.util.Properties;

public class ApplicationContext {

	Properties prop = null;

	public String getBean(String beanName)  {
		String result = null;
		prop = new Properties();
		
		try {
			prop.load(this.getClass().getResourceAsStream("/fr/datasense/formation/conf/application.properties"));
			result =  prop.getProperty(beanName);
		} catch (IOException e) {
				
		}
		return result;
	}

}
