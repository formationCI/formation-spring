package fr.datasense.formation.ria;

import java.io.IOException;
import java.util.Properties;

import fr.datasense.formation.spring.metier.IMetier;
import fr.datasense.formation.spring.metier.MetierImpl;

public class Presentation {
	

	
	public static void main(String[] args) {
	
		IMetier metier = new MetierImpl();
		System.out.println(metier.calculPrixTTC());
		
	}

	public static String getBean(String name) {
		Properties prop = new Properties();
		try {
			prop.load(Presentation.class.getResourceAsStream("/fr/datasense/formation/conf/application.properties"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return prop.getProperty(name);
		
	}

	
}
