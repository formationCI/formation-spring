package fr.datasense.formation.spring.cxf.config;

import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import fr.datasense.formation.spring.cxf.service.IRechecheFilm;

@Configuration
@ComponentScan(basePackages= {"fr.*"})
public class ClientConfiguration {
    @Bean(name = "client")
    public Object generateProxy() {
        return proxyFactoryBean().create();
    }

    
    @Bean
    public JaxWsProxyFactoryBean proxyFactoryBean() {
        JaxWsProxyFactoryBean proxyFactory = new JaxWsProxyFactoryBean();
        proxyFactory.setServiceClass(IRechecheFilm.class);
        proxyFactory.setAddress("http://localhost:8085/services/film");
        return proxyFactory;
    }
}