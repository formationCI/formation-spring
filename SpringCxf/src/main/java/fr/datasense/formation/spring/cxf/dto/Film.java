package fr.datasense.formation.spring.cxf.dto;

public class Film {
	
     private String nom;
     private String realisateur;
	 private int duree;
	
	public Film(){
		
	}
	

	public String getRealisateur() {
		return realisateur;
	}


	public void setRealisateur(String realisateur) {
		this.realisateur = realisateur;
	}


	public String getNom() {
		return nom;
	}
	
	
	public void setNom(String nom) {
		this.nom = nom;
	}
	public int getDuree() {
		return duree;
	}
	

	public void setDuree(int duree) {
		this.duree = duree;
	}
	
	
		
	@Override
	public String toString() {
		return "Film [nom=" + nom + ", duree=" + duree + "]";
	}
		

}
