package fr.datasense.formation.spring.cxf.service;

import java.util.Random;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import org.fluttercode.datafactory.impl.DataFactory;

import fr.datasense.formation.spring.cxf.dto.Film;



public class RechercheFilmService implements IRechecheFilm {
   
    /* (non-Javadoc)
	 * @see fr.datasense.formation.spring.cxf.service.IRechecheFilm#rechercherFilm(java.lang.Long)
	 */
    @Override
    public Film rechercherFilm( Long idFilm) {
    	    DataFactory df = new DataFactory();
    	    Random rand = new Random();
    		Film f = new Film();
    		f.setNom(df.getRandomText(4, 23));
    		f.setRealisateur(df.getFirstName() + " "+ df.getLastName() );
    		f.setDuree(rand.nextInt((300-100) +1) + 100);
    		return f;
    }

    
}
