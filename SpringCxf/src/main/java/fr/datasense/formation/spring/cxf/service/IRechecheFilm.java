package fr.datasense.formation.spring.cxf.service;

import fr.datasense.formation.spring.cxf.dto.Film;

/**
 * Ref : http://cxf.apache.org/docs/writing-a-service-with-spring.html
 * https://cwiki.apache.org/confluence/display/CXF20DOC/JAX-WS+Configuration
 * 
 * PART 1
 *  Cette interface doit etre un webservice donc etre décoré en conséquence.
 *  Elle representera le point d'entrée du WS : wsdl:portType
 *  l'input du WS est un id, afin que cette signature soit lisible dans la wsdl 
 *  id sera  transformé en nom plus explicite : id_film. 
 * 
 * 
 * PART 2 
 * L'implementation doit egalement être decoree: 
 * On retrouvera dans cette decoration le nom du webservice, le endpointInterface qui fera référence à cette interface  ainsi que le nom de l'operation du WS 
 * 
 * 
 * PART 3
 * L'objet de retour du WS est un objet type FILM
 * Cet objet doit être transformé en XML par cxf : voir l'api javax.xml.bind.annotation.*
 * 
 * 
 * PART 4
 * On doit creer la configuration pour injecter l'objet Endpoint de cxf qui, à partir du endpointInterface défini à l'étape 2, créera notre contrat de service
 * On definira egalement l'adresse  local ou sera publier ce WS : http://localhost:8085/services/film
 * 
 * PART 5
 * Un WS peut exister que dans un context web !!
 * Initialisons  Spring dans un context web, en lui spécifiant la configuration de l'étape 4
 * 
 * 
 * C'est fini :-) ...on a pas oublie quelque chose?????????? 
 * 
 *  
 * 
 * @author ismael
 *
 */
public interface IRechecheFilm { 
	
	Film rechercherFilm( Long id);
	
}