CREATE TABLE IF NOT EXISTS  film (
 id_film  INTEGER PRIMARY KEY auto_increment,
 nom  VARCHAR(30),
 realisateur VARCHAR(30),
 duree integer
);