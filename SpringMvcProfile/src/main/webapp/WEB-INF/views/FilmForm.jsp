<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
	"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>New/Edit Film</title>
</head>
<body>
	<div align="center">
		<h1>New/Edit Film</h1>
		<form:form action="saveFilm" method="post" modelAttribute="film">
		<table>
			<form:hidden path="id"/>
			<tr>
				<td>Nom Film:</td>
				<td><form:input path="nom" /></td>
			</tr>
			<tr>
				<td>Réalisateur:</td>
				<td><form:input path="realisateur" /></td>
			</tr>
			<tr>
				<td>Durée:</td>
				<td><form:input path="duree" /></td>
			</tr>
			
			<tr>
				<td colspan="2" align="center"><input type="submit" value="enregistrer"></td>
			</tr>
		</table>
		</form:form>
	</div>
</body>
</html>