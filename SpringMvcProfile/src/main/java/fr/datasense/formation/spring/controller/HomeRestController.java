package fr.datasense.formation.spring.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import fr.datasense.formation.spring.dao.FilmDAO;
import fr.datasense.formation.spring.model.Film;

@RestController
@RequestMapping(value="/api")
public class HomeRestController {
	
	@Autowired
	private FilmDAO filmDAO;
	
	@RequestMapping(value="/films")
	public ResponseEntity<List<Film>>  getFilms() throws IOException{
		List<Film> listFilm = filmDAO.list();
		 return new ResponseEntity<List<Film>>(listFilm, HttpStatus.OK);
	}
	
	//todo tester http://localhost:8080/SpringMvcJdbcTemplate/api/film/27.xml
	//activer l'annotation //@XmlRootElement sur Film
	@RequestMapping(value = "/films/{id}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE,MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<Film> getFilm(@PathVariable("id") int id) {
        System.out.println("Recherche de film avec l'id " + id);
        Film singleFilm = filmDAO.get(id);
        if (singleFilm == null) {
            System.out.println("LE film avec l'id" + id + " non trouvé");
            return new ResponseEntity<Film>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Film>(singleFilm, HttpStatus.OK);
    }
	
	/**
	 * 
		POST : http://localhost:8080/SpringMvcJdbcTemplate/api/film/ avec le slash 
		puis verifier ds header le champs localisation
	 * @param film
	 * @param ucBuilder
	 * @return
	 */
	  @RequestMapping(value = "/films/", method = RequestMethod.POST)
	    public ResponseEntity<Void> createFilm(@RequestBody Film film, UriComponentsBuilder ucBuilder) {
	        System.out.println("Enregistrement du film  " + film.getNom());
	 
	        //si existance du film a coder
	        //    return new ResponseEntity<Void>(HttpStatus.CONFLICT);
	        long pk =  filmDAO.saveOrUpdate(film);
	        HttpHeaders headers = new HttpHeaders();
	        headers.setLocation(ucBuilder.path("/film/{id}").buildAndExpand(pk).toUri());
	        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	    }

}
