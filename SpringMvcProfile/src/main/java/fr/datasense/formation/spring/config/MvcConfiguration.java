package fr.datasense.formation.spring.config;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.init.DatabasePopulator;
import org.springframework.jdbc.datasource.init.DatabasePopulatorUtils;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

@Configuration
@ComponentScan(basePackages="fr.datasense.formation.spring")
@EnableWebMvc
public class MvcConfiguration extends WebMvcConfigurerAdapter{

	
	@Bean
	public ViewResolver getViewResolver(){
		InternalResourceViewResolver resolver = new InternalResourceViewResolver();
		resolver.setPrefix("/WEB-INF/views/");
		resolver.setSuffix(".jsp");
		return resolver;
	}
	
	@Override 
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
	}


//http://www.h2database.com/html/features.html#execute_sql_on_connection
	@Bean(name="h2")
	@Profile("dev")
	public DataSource getDataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName("org.h2.Driver");
		//dataSource.setUrl("jdbc:h2:mem:test;INIT=runscript from 'classpath:/sql/create-db.sql'");
		dataSource.setUrl("jdbc:h2:~/filmDB");
		dataSource.setUsername("sa");
		dataSource.setPassword("sa");
		
		Resource initSchema = new ClassPathResource("create-db.sql");
		   DatabasePopulator databasePopulator = new ResourceDatabasePopulator(initSchema);
		   DatabasePopulatorUtils.execute(databasePopulator, dataSource);
		return dataSource;
	}
	
	@Bean(name="mySQL", destroyMethod = "close")
	@Profile("prod")
     public  DataSource dataSourceMySQL() {
        HikariConfig dataSourceConfig = new HikariConfig();
        dataSourceConfig.setDriverClassName("com.mysql.jdbc.Driver");
        dataSourceConfig.setJdbcUrl("jdbc:mysql://localhost:3306/db_film");
        dataSourceConfig.setUsername("root");
        dataSourceConfig.setPassword("root");
 
        return new HikariDataSource(dataSourceConfig);
    }
	
	
	/**
	 * Si on a qu'une seul data source on utilisera cette facon de faire
	 * car ca evite d'injecter la datasource par la suite
	 * @return
	 */
//	@Bean
//	public JdbcTemplate getJdbcTemplate() {
//	return new JdbcTemplate(getDataSource());	
//	}
	


}
