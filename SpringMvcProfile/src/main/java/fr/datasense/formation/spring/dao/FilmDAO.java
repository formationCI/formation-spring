package fr.datasense.formation.spring.dao;

import java.util.List;

import fr.datasense.formation.spring.model.Film;


public interface FilmDAO {
	
	public long saveOrUpdate(Film film);
	
	public void delete(int filmId);
	
	public Film get(int filmId);
	
	public List<Film> list();
}
