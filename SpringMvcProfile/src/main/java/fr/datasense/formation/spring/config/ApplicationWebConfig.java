package fr.datasense.formation.spring.config;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import javax.servlet.ServletRegistration.Dynamic;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

public class ApplicationWebConfig implements WebApplicationInitializer {

@Override
public void onStartup(ServletContext servletContext) throws ServletException {

    AnnotationConfigWebApplicationContext rootContext = new AnnotationConfigWebApplicationContext();
    rootContext.register(MvcConfiguration.class);

    servletContext.addListener(new ContextLoaderListener(rootContext));

    AnnotationConfigWebApplicationContext dispatcherContext = new AnnotationConfigWebApplicationContext();
    dispatcherContext.register(MvcConfiguration.class);

    Dynamic netskolaDispatcher = servletContext.addServlet("dispatcher",new DispatcherServlet(dispatcherContext));
    netskolaDispatcher.setLoadOnStartup(1);
    netskolaDispatcher.addMapping("/");
    
    ServletRegistration.Dynamic h2Servlet = servletContext.addServlet("h2Servlet", new org.h2.server.web.WebServlet());
    h2Servlet.setLoadOnStartup(1);
    h2Servlet.addMapping("/h2/*");
    
    //une facon d'activer le profile
    //ou en argument de la jvm : -Dspring.profiles.active=dev
    servletContext.setInitParameter("spring.profiles.active", "prod");
}

}