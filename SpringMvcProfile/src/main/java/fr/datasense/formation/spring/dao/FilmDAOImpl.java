package fr.datasense.formation.spring.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import fr.datasense.formation.spring.model.Film;

@Repository
public class FilmDAOImpl implements FilmDAO {
	
	//si on utilise une seule datadource 
	//on peu injecter le jdbc en le configurant dans la classe de config.
	//@Autowired
	private JdbcTemplate jdbcTemplate;
	
	//cette methode si on utilise le qualifier.
	@Autowired
	public FilmDAOImpl(DataSource dataSource) {
		jdbcTemplate = new JdbcTemplate(dataSource);
	}

	
	public FilmDAOImpl(){

	}

	@Override
	public long saveOrUpdate(Film film) {
		if (film.getId() > 0) {
			// update
			String sql = "UPDATE film SET nom=?, realisateur=?, duree=?  "
						+ "  WHERE id_film=?";
			return jdbcTemplate.update(sql, film.getNom(), film.getRealisateur(),
					film.getDuree(), film.getId());
		} else {
			//TODO : fournir le code commenter puis en TP demander de 
			//retourner l'id apres insert pour etre utiliser ds la location du header (REST api)
			
			// insert	
//			String sql = "INSERT INTO film (nom, realisateur, duree)"
//						+ " VALUES (?, ?, ?)";
//			int id = jdbcTemplate.update(sql, film.getNom(), film.getRealisateur(),
//					film.getDuree());
//			System.out.println("creation avec id  " +id );
		
			SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(this.jdbcTemplate).withTableName("film").usingGeneratedKeyColumns("id_film");
			final Map<String, Object> parameters = new HashMap<>();
		    parameters.put("nom", film.getNom()); 
		    parameters.put("realisateur", film.getRealisateur());
		    parameters.put("duree",film.getDuree());
		    final Number key = simpleJdbcInsert.executeAndReturnKey(parameters);
		    final long pk = key.longValue();
		   return pk;
			
		}
		

		
	}

	@Override
	public void delete(int filmId) {
		String sql = "DELETE FROM film WHERE id_film=?";
		jdbcTemplate.update(sql, filmId);
	}

	@Override
	public List<Film> list() {
		String sql = "SELECT * FROM film";
		List<Film> listFilm = jdbcTemplate.query(sql, new RowMapper<Film>() {

			@Override
			public Film mapRow(ResultSet rs, int rowNum) throws SQLException {
				Film film = new Film();
	
				film.setId(rs.getInt("id_film"));
				film.setNom(rs.getString("nom"));
				film.setRealisateur(rs.getString("realisateur"));
				film.setDuree(rs.getInt("duree"));
				
				return film;
			}
			
		});
		
		return listFilm;
	}

	@Override
	public Film get(int id_film) {
		String sql = "SELECT * FROM film WHERE id_film=" + id_film;
		return jdbcTemplate.query(sql, new ResultSetExtractor<Film>() {

			@Override
			public Film extractData(ResultSet rs) throws SQLException,
					DataAccessException {
				if (rs.next()) {
					Film film = new Film();
					film.setId(rs.getInt("id_film"));
					film.setNom(rs.getString("nom"));
					film.setRealisateur(rs.getString("realisateur"));
					film.setDuree(rs.getInt("duree"));
					return film;
				}
				
				return null;
			}
			
		});
	}

}
