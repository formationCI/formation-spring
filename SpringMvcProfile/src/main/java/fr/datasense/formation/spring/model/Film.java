package fr.datasense.formation.spring.model;

import javax.xml.bind.annotation.XmlRootElement;


//si l'annotation pas activer erreur 404
@XmlRootElement
public class Film {
	private int id;
	private String nom;
	private String realisateur;
	private int duree;

	public Film() {
	}

	public Film(String name, String realisateur, int duree) {
		this.nom = name;
		this.realisateur = realisateur;
		this.duree = duree;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String name) {
		this.nom = name;
	}

	public String getRealisateur() {
		return realisateur;
	}

	public void setRealisateur(String realisateur) {
		this.realisateur = realisateur;
	}

	public int getDuree() {
		return duree;
	}

	public void setDuree(int duree) {
		this.duree = duree;
	}
}
