package fr.datasense.formation.spring.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import fr.datasense.formation.spring.dao.FilmDAO;
import fr.datasense.formation.spring.model.Film;

@Controller
public class HomeController {

	@Autowired
	private FilmDAO filmDAO;

	//http://localhost:8080/SpringMvcProfile/
	@RequestMapping(value = "/")
	public ModelAndView listFilm(ModelAndView model) throws IOException {
		List<Film> listFilm = filmDAO.list();
		model.addObject("listFilm", listFilm);
		model.setViewName("home");

		return model;
	}

	@RequestMapping(value = "/newFilm", method = RequestMethod.GET)
	public ModelAndView createFilm(ModelAndView model) {
		Film film = new Film();
		model.addObject("film", film);
		model.setViewName("FilmForm");
		return model;
	}

	@RequestMapping(value = "/saveFilm", method = RequestMethod.POST)
	public ModelAndView saveFilm(@ModelAttribute Film film) {
		filmDAO.saveOrUpdate(film);
		return new ModelAndView("redirect:/");
	}

	@RequestMapping(value = "/deleteFilm", method = RequestMethod.GET)
	public ModelAndView deleteFilm(HttpServletRequest request) {
		int contactId = Integer.parseInt(request.getParameter("id"));
		filmDAO.delete(contactId);
		return new ModelAndView("redirect:/");
	}

	@RequestMapping(value = "/editFilm", method = RequestMethod.GET)
	public ModelAndView editFilm(HttpServletRequest request) {
		int filmId = Integer.parseInt(request.getParameter("id"));
		Film film = filmDAO.get(filmId);
		ModelAndView model = new ModelAndView("FilmForm");
		model.addObject("film", film);

		return model;
	}

	// TODO : methode a faire implementer en tp
	// http://localhost:8080/SpringMvcJdbcTemplate/search?idFilm=26
	@RequestMapping(value = "/search", params = { "idFilm" })
	public ModelAndView rechercheFilmById(@RequestParam(value = "idFilm") String search, ModelAndView model) {
		Film f = filmDAO.get(Integer.parseInt(search));
		List<Film> listFilm = new ArrayList<>();
		model.addObject("listFilm", listFilm);
		model.setViewName("home");
		if (f == null) {
			return model;
		}
		listFilm.add(f);
		return model;
	}

}
