package fr.datasense.formation.ria;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

//============		
//EXO 1
//============
//Configurer cette classe comme classe de 
//-configuration
//-scan du package
//-prise en charge des properties
@Configuration
@ComponentScan(basePackages= {"fr.*"})
@PropertySource("classpath:data.properties")
public class ConfigApp {

	//============		
	//EXO 2
	//============
	// creer le bean PropertySourcesPlaceholderConfigurer
	
	
	
	//============		
	//EXO 3
	//============
	// creer le bean MetierImpl

		
}
