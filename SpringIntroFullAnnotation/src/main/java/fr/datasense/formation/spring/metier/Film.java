package fr.datasense.formation.spring.metier;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Film {
	
	@Value("${film.nom}")
	private String nom;
	@Value("${film.duree}")
	private Integer duree;
	
	public Film(){
		
	}
	
	public Film(String nom, Integer duree) {
		super();
		this.nom = nom;
		this.duree = duree;
	}
	public String getNom() {
		return nom;
	}
	
	
	public void setNom(String nom) {
		this.nom = nom;
	}
	public Integer getDuree() {
		return duree;
	}
	

	public void setDuree(Integer duree) {
		this.duree = duree;
	}
	
	public void initIt() throws Exception {
		  System.out.println("Init method FILM");
		}

		public void cleanUp() throws Exception {
		  System.out.println("Spring Container destroy");
		}
		
		
	@Override
	public String toString() {
		return "Film [nom=" + nom + ", duree=" + duree + "]";
	}
		

}
