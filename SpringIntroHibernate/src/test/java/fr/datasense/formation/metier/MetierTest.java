package fr.datasense.formation.metier;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import fr.datasense.formation.ria.ConfigApp;
import fr.datasense.formation.spring.metier.IMetier;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ConfigApp.class})
public class MetierTest {
	

	@Autowired 
	IMetier metier;
	
	@Test
	public void testMetierWithDaoImpl(){
		ReflectionTestUtils.setField(metier, "daoBean", "daoImpl");
		Assert.assertEquals(metier.calculPrixTTC(), 240.0,0.0);
	}
	
	@Test
	public void testMetierWithDaoImpl2(){
		ReflectionTestUtils.setField(metier, "daoBean", "daoImpl2");
		Assert.assertEquals(metier.calculPrixTTC(), 54.0,0.0);
	}

}
