package fr.datasense.formation.spring.metier;



import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;

import fr.datasense.formation.spring.dao.DaoFactory;
import fr.datasense.formation.spring.dao.IDao;


public class MetierImpl implements IMetier, InitializingBean{
	

	
	@Value("${monDao}")
	private String daoBean;
	
	@Autowired
	private DaoFactory daoFactory;
	
	public String getDaoBen() {
		return daoBean;
	}


	public void setDaoBen(String daoBen) {
		this.daoBean = daoBen;
	}
	@Transactional
	public double calculPrixTTC() {
		IDao result = daoFactory.getDaoImp(getDaoBen());
		return result.getPrixHT() * 1.2d;
	}
	
	public void afterPropertiesSet() throws Exception {
		System.out.println("init classe MetierImp");
		
	}
}
