package fr.datasense.formation.ria;

import java.sql.SQLException;
import java.util.Properties;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.beans.factory.config.ServiceLocatorFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import fr.datasense.formation.spring.dao.DaoFactory;
import fr.datasense.formation.spring.dao.DaoH2BaseImpl;
import fr.datasense.formation.spring.dao.DaoImpl;
import fr.datasense.formation.spring.dao.DaoImpl2;
import fr.datasense.formation.spring.metier.IMetier;
import fr.datasense.formation.spring.metier.MetierImpl;

@Configuration
@ComponentScan(basePackages = { "fr.*" })
@EnableTransactionManagement
@PropertySource("classpath:data.properties")
public class ConfigApp {
	
//	==========
//	EXO 
//	==========
//	-Creer une nouvelle implementation de Idao.
//	-Cette implementation récuperera via une Entity JPA un prix en base de donnee
//	-Injecter une donnee en base lorsque le bean a été intialiser
//	-Completer la conf du datasource pour récuperer les infos necessaire  d'acces à base de donnée via un fichier
//	de properties. Pour info voici le driver et l'url d'acces a la base H2 db.driver=org.h2.Driver , jdbc:h2:mem:test
//	-Completer la conf de l'entityManager pour lui donner le nom de l'unité de persistance
	
	 @Autowired
	 private Environment environment;

	@Bean
	public IMetier getMetier() {
		return new MetierImpl();
	}
	
	@Bean 
	public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
	return new PropertySourcesPlaceholderConfigurer();
	}

	@Bean
	public FactoryBean serviceLocatorFactoryBean() {
		ServiceLocatorFactoryBean factoryBean = new ServiceLocatorFactoryBean();
		factoryBean.setServiceLocatorInterface(DaoFactory.class);
		return factoryBean;
	}

	@Bean(name = "daoImpl")
	@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
	public DaoImpl daoImpl() {
		return new DaoImpl();
	}

	@Bean(name = "daoImpl2")
	@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
	public DaoImpl2 daoImpl2() {
		return new DaoImpl2();
	}

	
	@Bean(name ="daoH2BaseImpl")
	@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
	public DaoH2BaseImpl daoH2BaseImpl() {
		return new DaoH2BaseImpl();
	}
	 
	@Bean(destroyMethod = "close")
    DataSource dataSource(Environment env) {
        HikariConfig dataSourceConfig = new HikariConfig();
        dataSourceConfig.setDriverClassName(env.getRequiredProperty("db.driver"));
       //TODO 
 
        return new HikariDataSource(dataSourceConfig);
    }
	    
	   
	    @Bean
	    LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource, 
	                                                                Environment env) {
	        LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
	        entityManagerFactoryBean.setDataSource(dataSource);
	        entityManagerFactoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
	        entityManagerFactoryBean.setPackagesToScan("fr.*");
	 
	        Properties jpaProperties = new Properties();
	        jpaProperties.put("hibernate.dialect", env.getRequiredProperty("hibernate.dialect"));
	        jpaProperties.put("hibernate.hbm2ddl.auto", 
	                env.getRequiredProperty("hibernate.hbm2ddl.auto")
	        );

	   
	        jpaProperties.put("hibernate.show_sql", 
	                env.getRequiredProperty("hibernate.show_sql")
	        );
	
	        jpaProperties.put("hibernate.format_sql", 
	                env.getRequiredProperty("hibernate.format_sql")
	        );
	        
	        entityManagerFactoryBean.setJpaProperties(jpaProperties);

	        //TODO
	        return entityManagerFactoryBean;
	    }

	    @Bean
	    @Autowired
	    JpaTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
	        JpaTransactionManager transactionManager = new JpaTransactionManager();
	        transactionManager.setEntityManagerFactory(entityManagerFactory);
	        return transactionManager;
	    }
	    
	    
	    @Bean(initMethod="start",destroyMethod="stop")
	    public org.h2.tools.Server h2WebConsonleServer () throws SQLException {
	      return org.h2.tools.Server.createWebServer("-web","-webAllowOthers","-webDaemon","-webPort", "8082");
	    }
}
