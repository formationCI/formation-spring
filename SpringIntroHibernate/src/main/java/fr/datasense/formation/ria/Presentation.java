package fr.datasense.formation.ria;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import fr.datasense.formation.spring.metier.Film;
import fr.datasense.formation.spring.metier.IMetier;


public class Presentation {


	

	public static void main(String[] args) throws Exception {
		
		
		 AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
		 ctx.register(ConfigApp.class);
		 ctx.refresh();
		 
		 IMetier metier =  ctx.getBean(IMetier.class);
		 
		 System.out.println(metier.calculPrixTTC());
		 Film f = ctx.getBean(Film.class);
		 System.out.println(f);
		 
		 ctx.close();
	}

}
