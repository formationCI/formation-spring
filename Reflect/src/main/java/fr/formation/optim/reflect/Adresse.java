package fr.formation.optim.reflect;

public class Adresse {
	
	private String rue;
	private int codePostal;
	private float x;
	
	public float getX() {
		return x;
	}
	public void setX(float x) {
		this.x = x;
	}
	public String getRue() {
		return rue;
	}
	public void setRue(String rue) {
		this.rue = rue;
	}
	public int getCodePostal() {
		return codePostal;
	}
	public void setCodePostal(int codePostal) {
		this.codePostal = codePostal;
	}
	
	

}
