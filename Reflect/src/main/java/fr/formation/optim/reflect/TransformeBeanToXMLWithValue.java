package fr.formation.optim.reflect;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
/**
 * Intro à l'API Reflect 
 * @author ismael
 *
 */
public class TransformeBeanToXMLWithValue {
	
	public static void perform(Object o) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		
		// recuperer tous les champs de la classes 
		//pour chauque champs verifier son type : dans notre cas et par simplification 
		//on verifiera le type string , primitif 
		//qd il s'agira d'un object il faudra faire du recurssif 
		
		
		
		
		
	}
	
	
	private static void writeData(Object o ,Class clazz , Field f) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException{
		Method m = clazz.getMethod("get"+f.getName().substring(0, 1).toUpperCase()+""+f.getName().substring(1, f.getName().length()));
    	Object invoke = m.invoke(o);
    	if( invoke!=null){
    		System.out.println("<"+f.getName()+">"+invoke.toString()+"</"+f.getName()+">");
    	}
	}
	

	
	public static void main(String[] args) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		Personne p = new Personne();
		p.setAge(33);
		p.setPrenom("Toto");
		Adresse a = new Adresse();
		a.setRue("62 rue des limites");
		a.setCodePostal(94160);
		a.setX(45f);
		p.setAdresse(a);
		p.setTaille(1.78f);
	
		TransformeBeanToXMLWithValue.perform(p);
	}

}
