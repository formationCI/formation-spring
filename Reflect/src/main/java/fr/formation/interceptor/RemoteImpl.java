package fr.formation.interceptor;

public class RemoteImpl implements Remote{

	@Override
	public void callRemote(String pwd) {
		System.out.println("connexion reseaux avec le mot de passe "+ pwd);
	}

	@Override
	public void callRemoteSecure(String pwd) {
		System.out.println("connexion reseaux avec le mot de passe "+ pwd);
	}

	@Override
	public void call() {
		System.out.println("une operation.");
		
	}
	
	

}
