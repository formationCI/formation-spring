package fr.formation.interceptor;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * La proxification en java utilisé par bon nombre d'API : ex hibernate, spring ...Etc
 * Spring utilise plutot CGlib mais le principe reste le meme.
 * @author ismael
 *
 */
public class RemoteHandler implements InvocationHandler {
	
	private Remote remote;
	
	public RemoteHandler(Remote r){
		remote=r;
	}
	
	/**
	 * intercepte toutes les methodes de Remote
	 * puis efectue une operation particuliere si la méthode est callRemoteSecure
	 */
	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
			 
//		partie 1 
//		=============
//		 si le nom de la méthode est egale à callRemoteSecure
//		 recuperer les arguments de la méthode sous forme de tableau de char, il n'ya qu'un seul argument donc : arg[0]
//		 pour chaque element du tableau remplacer par un * sauf les 3 derniers caractères
//		 invoquer la methode avec l'argument modifier
		
		
//		Partie 2
//		==============
//		pour la methode callRemote avec argumentinvoquer simplement la methode
		
//		Partie 3 
//		==============
//		pour la méthode sans argument, ne rien faire, on ne prend pas en compte le compte tenu de la méthode réelle 
		
		
		
		
		return null;
	}
	
	public Remote process(){
		
		//utiliser l'appel du proxy cf java doc Proxy.newProxyInstance(...) : https://docs.oracle.com/javase/8/docs/api/java/lang/reflect/Proxy.html
		return null;
	}
	
	
	public static void main(String[] args) {
		
		RemoteImpl remote = new RemoteImpl() ;
		RemoteHandler handler = new RemoteHandler(remote);
		Remote remoteProxy = handler.process();
		remoteProxy.callRemote("unmotdepassetreslong");
		remoteProxy.callRemoteSecure("unmotdepassecrypteretlong2");
		remoteProxy.call();
		
		//pour aller plus loin voir la classe java.lang.instrument.Instrumentation
		//http://igm.univ-mlv.fr/~dr/XPOSE2010/MockingenJava/strategie2.html
		//http://blog.javabenchmark.org/2013/05/java-instrumentation-tutorial.html
	}

}
