package fr.formation.interceptor;

public interface Remote {
	
  public void callRemoteSecure(String pwd);
  
  public void callRemote(String pwd);
  
  public void call();
}
