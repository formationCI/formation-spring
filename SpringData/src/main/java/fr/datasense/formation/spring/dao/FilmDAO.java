package fr.datasense.formation.spring.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import fr.datasense.formation.spring.model.Film;


public interface FilmDAO extends CrudRepository<Film,Integer> {
	List<Film> findByRealisateur(@Param("nom") String nom);
	List<Film> findByDureeGreaterThan(@Param("duree") int duree);
	
	@Query("select f from Film f where f.nom='Xman'")
	Film findXmanFilm();
}
