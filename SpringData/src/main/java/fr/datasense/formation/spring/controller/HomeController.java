package fr.datasense.formation.spring.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import fr.datasense.formation.spring.dao.FilmDAO;
import fr.datasense.formation.spring.model.Film;
import fr.datasense.formation.spring.service.FilmService;

@Controller
public class HomeController {

	// TODO ajouter une couche service lors des TPs
	@Autowired
	private FilmService service;

	@RequestMapping(value = "/")
	public ModelAndView listFilm(ModelAndView model) throws IOException {
		List<Film> listFilm = service.findAll();
		model.addObject("listFilm", listFilm);
		model.setViewName("home");

		return model;
	}

	@RequestMapping(value = "/newFilm", method = RequestMethod.GET)
	public ModelAndView createFilm(ModelAndView model) {
		Film film = new Film();
		model.addObject("film", film);
		model.setViewName("FilmForm");
		return model;
	}

	@RequestMapping(value = "/saveFilm", method = RequestMethod.POST)
	public ModelAndView saveFilm(@ModelAttribute Film film) {
		service.save(film);
		return new ModelAndView("redirect:/");
	}

	@RequestMapping(value = "/deleteFilm", method = RequestMethod.GET)
	public ModelAndView deleteFilm(HttpServletRequest request) {
		int idFilm = Integer.parseInt(request.getParameter("id"));
		service.deleteFilm(idFilm);
		return new ModelAndView("redirect:/");
	}

	@RequestMapping(value = "/editFilm", method = RequestMethod.GET)
	public ModelAndView editFilm(HttpServletRequest request) {
		int filmId = Integer.parseInt(request.getParameter("id"));
		Film film = service.searchFilmById(filmId);
		ModelAndView model = new ModelAndView("FilmForm");
		model.addObject("film", film);

		return model;
	}

	// TODO : methode a faire implementer en tp
	// http://localhost:8080/SpringMvcJdbcTemplate/search?idFilm=XX
	@RequestMapping(value = "/search", params = { "idFilm" })
	public ModelAndView rechercheFilmById(@RequestParam(value = "idFilm") String search, ModelAndView model) {
		List<Film> listFilm =  new ArrayList<>();
		model.addObject("listFilm", listFilm);
		model.setViewName("home");
		listFilm.add(service.searchFilmById(Integer.parseInt(search)));
		return model;
	}
	
	// TODO : methode a faire implementer en tp
	// http://localhost:8080/SpringMvcJdbcTemplate/searchRealisateur/isma
	@RequestMapping(value = "/searchRealisateur/{nom}")
	public ModelAndView rechercheFilmByRealisateur(@PathVariable(value = "nom") String search, ModelAndView model) {
		List<Film> listFilm = new ArrayList<>();
		model.addObject("listFilm", listFilm);
		model.setViewName("home");
		listFilm.addAll(service.searchRealisateur(search));
		return model;
	}
	
	// TODO : methode a faire implementer en tp
		// http://localhost:8080/SpringMvcJdbcTemplate/searchDuree/90
		@RequestMapping(value = "/searchDuree/{duree}")
		public ModelAndView rechercheFilmByDureeSuperieurA(@PathVariable(value = "duree") int duree, ModelAndView model) {
			List<Film> listFilm = new ArrayList<>();
			model.addObject("listFilm", listFilm);
			model.setViewName("home");
			listFilm.addAll(service.searchByDuree(duree));
			return model;
		}

}
