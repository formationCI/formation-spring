package fr.datasense.formation.spring.service;

import java.util.List;

import fr.datasense.formation.spring.model.Film;

public interface FilmService {

	public List<Film> findAll();

	public int save(Film f);

	public Film searchFilmById(int id);
	
	public void  deleteFilm(int id);
	
	public List<Film> searchRealisateur(String nom);
	
	public List<Film> searchByDuree(int duree);
	
	public Film searchXman();
}
