package fr.datasense.formation.spring.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.datasense.formation.spring.dao.FilmDAO;
import fr.datasense.formation.spring.model.Film;

@Service
public class FilmServiceImpl implements FilmService{

	@Autowired
	private FilmDAO filmDao;
	
	@Override
	public List<Film> findAll() {
		Iterable<Film> findAll = filmDao.findAll();
		List<Film> target = new ArrayList<>();
		findAll.forEach(target::add);
		return target;
	}
	
	@Override
	public int save(Film f) {
		Film result = filmDao.save(f);
		return result.getId();
	}

	@Override
	public Film searchFilmById(int id) {
		return filmDao.findOne(id);
	}

	@Override
	public void deleteFilm(int id) {
		filmDao.delete(id);
		
	}

	@Override
	public List<Film> searchRealisateur(String nom) {
		return filmDao.findByRealisateur(nom);
	}

	@Override
	public List<Film> searchByDuree(int duree) {
		return filmDao.findByDureeGreaterThan(duree);
		//return null;
	}

	@Override
	public Film searchXman() {
		return filmDao.findXmanFilm();
	}


}
