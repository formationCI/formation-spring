<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Film Manager</title>
    </head>
    <body>
    	<div align="center">
	        <h1>Contact List</h1>
	        <h3><a href="newFilm">Ajout Film</a></h3>
	        <table border="1">
	        	<th>No</th>
	        	<th>Nom</th>
	        	<th>Réalisateur</th>
	        	<th>Duree</th>
	      
	        	
				<c:forEach var="film" items="${listFilm}" varStatus="status">
	        	<tr>
	        		<td>${status.index + 1}</td>
					<td>${film.nom}</td>
					<td>${film.realisateur}</td>
					<td>${film.duree}</td>
				
					<td>
						<a href="editFilm?id=${film.id}">Edit</a>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<a href="deleteFilm?id=${film.id}">Delete</a>
					</td>
							
	        	</tr>
				</c:forEach>	        	
			</table>
    	</div>
    </body>
</html>
