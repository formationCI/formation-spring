package fr.datasense.formation.ria;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

import fr.datasense.formation.spring.metier.Film;
import fr.datasense.formation.spring.metier.IMetier;
import fr.datasense.formation.spring.metier.MetierAspect;
import fr.datasense.formation.spring.metier.MetierImpl;

@Configuration
@ComponentScan(basePackages = { "fr.datasense.*" })
@PropertySource("classpath:data.properties")
public class ConfigApp {

	@Bean 
	public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
	return new PropertySourcesPlaceholderConfigurer();
	}
	
	@Bean
	public IMetier getMetier() {
		return new MetierImpl();
	}
	

//	============		
//	EXO 1
//	============
//	Activer les aspect avec Spring 
//	Puis injecter la classe Aspect MetierAspect
	


		
}
