package fr.datasense.formation.ria;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

import fr.datasense.formation.spring.metier.Film;
import fr.datasense.formation.spring.metier.IMetier;
import fr.datasense.formation.spring.metier.MetierAspect;


public class Presentation {

	public static void main(String[] args) {
		 AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
		 ctx.register(ConfigApp.class);
		 ctx.refresh();
		 IMetier metier =  ctx.getBean(IMetier.class);
		 System.out.println(metier.calculPrixTTC());
		
		 Film f =  ctx.getBean(Film.class);
		 System.out.println("film " +f);
		 
		 
		 ctx.close();
	}

}
