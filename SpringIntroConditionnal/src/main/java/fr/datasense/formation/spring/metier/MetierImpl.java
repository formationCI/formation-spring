package fr.datasense.formation.spring.metier;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;

import fr.datasense.formation.spring.dao.IDao;

public class MetierImpl implements IMetier, InitializingBean{
	
	
	@Autowired
	private IDao dao;
	
	
	
	public double calculPrixTTC() {
	
		return dao.getPrixHT() * 1.2d;
	}
	
	public void afterPropertiesSet() throws Exception {
		System.out.println("init classe MetierImp");
		
	}
	
	
}
