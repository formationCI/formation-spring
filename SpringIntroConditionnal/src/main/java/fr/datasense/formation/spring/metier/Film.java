package fr.datasense.formation.spring.metier;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component(value="filmBean")
public class Film {
	
	@Value("${film.nom}")
	private String nom;
	@Value("${film.duree}")
	private int duree;
	
	public Film(){
	}
	
	public Film(String nom, int duree) {
		super();
		this.nom = nom;
		this.duree = duree;
	}
	public String getNom() {
		return nom;
	}
	
	
	public void setNom(String nom) {
		this.nom = nom;
	}
	public int getDuree() {
		return duree;
	}
	

	public void setDuree(int duree) {
		this.duree = duree;
	}
	
		
	@Override
	public String toString() {
		return "Film [nom=" + nom + ", duree=" + duree + "]";
	}
		

}
