package fr.datasense.formation.ria;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

import fr.datasense.formation.spring.metier.IMetier;
import fr.datasense.formation.spring.metier.MetierImpl;

@Configuration
@ComponentScan(basePackages = { "fr.*" })
@PropertySource("classpath:data.properties")
public class ConfigApp {

	@Bean
	public IMetier getMetier() {
		return new MetierImpl();
	}
	
	@Bean 
	public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
	return new PropertySourcesPlaceholderConfigurer();
	}

//	========
//	EXO 1
//	=========
//	Injecter les 2 implementation de IDao
//	On rajoutera la d�coration @@Conditional qui utilisera la condiion d�fini dans la classe DaoConditionnal
	
//	========
//	EXO 2
//	=========
//	D�finir la condition suivante dans la classe DaoConditionnal:
//	si l'argument de la JVM user.variant  (-Duser.variant=daoImpl2)
//	est �gale au nom du bean activ� la condition

}
