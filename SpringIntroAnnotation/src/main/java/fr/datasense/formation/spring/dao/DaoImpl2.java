package fr.datasense.formation.spring.dao;

import org.springframework.stereotype.Repository;

@Repository(value="daoImpl2Bean")
public class DaoImpl2 implements IDao{

	public double getPrixHT() {
		return 45d;
	}

}
