package fr.datasense.formation.ria;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.datasense.formation.spring.metier.Film;
import fr.datasense.formation.spring.metier.IMetier;


public class Presentation {
	
	

	public static void main(String[] args) {
//		============		
//		EXO 1
//		============
//		Ajoutez les classes pour que l'injection se fasse sans configuration
//		dans le fichier applicationContext.xml
		
		ApplicationContext context = new ClassPathXmlApplicationContext("fr/datasense/formation/conf/applicationContext.xml");
		IMetier metier = (IMetier) context.getBean("metierBean");
		System.out.println(metier.toString() + " "+ metier.calculPrixTTC());
		IMetier metier2 = (IMetier) context.getBean("metierBean");
		System.out.println(metier2.toString() + " "+ metier2.calculPrixTTC());
		
//		============		
//		EXO 2
//		============
//		-Les 2 implémentations de IDao doit se retrouver dans le beanFactory de Spring
//		-la classe métier doit injecter DaoImmpl2
		

//		============		
//		EXO 3
//		============
//		-Les 2 implémentations de IDao doit se retrouver dans le beanFactory de Spring
//		-la classe métier doit injecter  DaoImmpl sans préciser le nom du bean  
		
		
//		============		
//		EXO 4
//		============	
//		-La duree et le nom du film doit etre récuperer d'un fichier de properties puis etre injecter
//		dans les attributs de la classe Film
		
		
		Film f = (Film) context.getBean("filmBean");
		System.out.println(f);
	}

}
