package fr.datasense.formation.ria;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

import fr.datasense.formation.spring.metier.IMetier;
import fr.datasense.formation.spring.metier.MetierImpl;

@Configuration
@ComponentScan(basePackages = { "fr.*" })
@PropertySource("classpath:data.properties")
public class ConfigApp {

	@Bean
	public IMetier getMetier() {
		return new MetierImpl();
	}
	
	@Bean 
	public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
	return new PropertySourcesPlaceholderConfigurer();
	}

//	==========		
//	EXO 1
//	==========
//	-Definir une factory qui injectera l'implementation du bean Dao dans Metier 
//	(on tuilisera la factory DaoFactory)
//	-Le nom du bean � injecter devra �tre r�cuperer du fichier properties.
//	La classe Metier devra recuperer l'implementation du Dao via la factory
//	Indice: Spring dispose d'une implementation du pattern factory : ServiceLocatorFactoryBean.
	
	
}
