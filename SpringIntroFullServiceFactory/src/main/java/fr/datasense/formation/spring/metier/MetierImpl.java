package fr.datasense.formation.spring.metier;

import org.springframework.beans.factory.InitializingBean;

public class MetierImpl implements IMetier, InitializingBean{
	

	public double calculPrixTTC() {
		//TODO: ici on doit utiliser la factory 
		return  0d;
	}
	
	public void afterPropertiesSet() throws Exception {
		System.out.println("init classe MetierImp");
		
	}
	
	
}
