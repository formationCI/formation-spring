package fr.datasense.formation.spring.dao;

public interface DaoFactory {
	
	public IDao getDaoImp(String beanName);
	
}
