package fr.datasense.formation.dao;


import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.datasense.formation.ria.ConfigApp;
import fr.datasense.formation.spring.dao.DaoImpl;
import fr.datasense.formation.spring.dao.IDao;


public class DaoTest {
	

	IDao dao;

	
	@Test
	public void testDaoInstance(){
		if(dao instanceof DaoImpl){
			Assert.assertTrue(true);
		}
	}
	
	@Test
	public void testDaoPrix(){
		Assert.assertEquals(dao.getPrixHT(), 200.0,0.0);
		
	}
	
}
